#!/usr/bin/python

import cgi
import os.path

badge_dir = "/soe/elm/.html/classes/badges"
try:
    print ('Content-Type: image/svg+xml\n')
    form = cgi.FieldStorage ()
    data = dict()
    data['commitid'] = form.getvalue ('c')
    badge = form.getvalue ('f')
    if badge not in ('pass', 'fail', 'submitok', 'tests'):
        # Somewhat appropriate, right?
        badge = 'fail'
    fn = os.path.join (badge_dir, badge + '.svg')
    with open (fn, "r") as f:
        svgdata = f.read ()
    print (svgdata.format (**data))
except:
    pass

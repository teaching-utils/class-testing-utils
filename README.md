# Class testing utilities

This repo contains class testing utilities and configuration files. Most of these files can be hosted on any Web server, including departmental servers such as those available in BSOE.

# Group setup

You'll first need to set up a group for your class. You should have already done this if you followed instructions in [setting up your class for `git.ucsc.edu`](https://git.ucsc.edu/teaching-utils/class-setup-utils). For this example, we'll assume that the full group name is `cse130/fall20-miller`. This includes both the class name and the section / quarter info.

## Setting group variables

The full group name (and the one we'll be changing the settings for) is `cse130/fall20-miller`. Navigate to this group (search for it under the **Groups** menu), and select **Settings** -> **CI/CD**. Expand the section named **Variables**.

You're going to add five new variables to this section. Add a variable by clicking on **Add Variable**. The key is the name of the variable (e.g., `TESTING_BASE_URL`) and the value is the string that's assigned to the variable name. You won't need to check the **Protect variable** box, but you *will* need to check the **Mask variable** box for the variable containing a secret.

The five variables we need to add are:

* `TESTING_DOCKER_IMAGE`: This variable should be set to the Docker image you want to use to run your tests. `ethanlmiller/ubuntu2204-clang14:latest` has the necessary Python 3 libraries and a `clang-14` installation, but you can use any Docker image with the necessary Python 3 libraries in it.
* `TESTING_BASE_URL`: This variable should be set to the non-secret part of the URL in which you're storing the configuration files. For example, you might use `https://www.soe.ucsc.edu/~elm/classes/cse130`.
* `TESTING_SECRET`: **This variable must be masked, and should be hard to guess.** We recommend using at least 10 digits of a SHA1 hash of random data.
* `TESTING_API_TOKEN`: **This variable must be masked**. You'll need to generate a group API token that allows writing via the API. The value of this variable will be set to the token you generate.
* `TESTING_BADGE_URL`: This is the URL for the badge server that you'll need to set up. See below for details.

# Setting up a directory on your Web server

The `check_submission.py` script and `assignments.toml` file must be available at the URL `${TESTING_BASE_URL}/${TESTING_SECRET}`. Consider the following example:

* `TESTING_BASE_URL` = `https://www.soe.ucsc.edu/~cruzid/testing-stuff`
* `TESTING_SECRET` = `abcdef0123456789`

For this configuration, the `check_submission.txt` script would be fetched using the URL `https://www.soe.ucsc.edu/~cruzid/testing-stuff/abcdef0123456789/check_submission.py`. Similarly, `assignments.toml` would be fetched from `https://www.soe.ucsc.edu/~cruzid/testing-stuff/abcdef0123456789/assignments.toml`. **Obviously, choose a better secret than this one.**

Whichever directory you choose, you should set the permissions to the `TESTING_BASE_URL` directory to `0711`, which allows the Web server to go into the directory, but **disallows** listing the directory for anyone but the owner (that's you!). This way, nobody but you can list the directory to determine that `abcdef0123456789` is the name of your secret directory. You can create the secret directory and set permissions by running these commands (using the example directory on one of the `*dance` machines):

    cd ~/.html/testing-stuff
    chmod 711 .
    mkdir abcdef0123456789
    chmod 711 abcdef0123456789

Next, place the `check_submission.py` and `assignments.toml` files into the `abcdef0123456789` directory. Make them world-readable by doing:

```
chmod 644 check_submission assignment-testing.toml
```

The only thing left to do on the server side is edit the `assignments.toml` file to contain
the appropriate configuration for your class. Documentation on the configuration options is in the
file itself, in comments.

**IMPORTANT:** You may make edits to the `assignments.toml` file whenever you like, even after the quarter has started. Each time a test runs, it fetches the most current `assignments.toml` from the server, so changes you make mid-quarter will be reflected as soon as they're available from the server.

**NOTE:** The UCSC SOE server wants to consider any `.py` file to be a script. This isn't what you want, so you probably shouldn't name your script file *as stored on the server* with a `.py` extension. Instead, call it something like `check_submission.txt`. Then, rename it to `check_submission.py` in your `.gitlab-ci.yml` file, as described below.

# Setting up .gitlab-ci.yml

A file called `.gitlab-ci.yml` must be in *each* student's repo to enable automatic testing. The good news is that this file doesn't need much configuration; you can just put the contents of the `gitlab-ci.yml` file in this repo into every student's repo. Note that, while the file in this repo doesn't start with `.`, the file in a student's repo *must* be named `.gitlab-ci.yml`, including the initial `.`. The `gitlab-ci.yml` file provided here uses the renaming of `check_submission.txt` to `check_submission.py` trick described above. You just fetch the file using `curl` and whatever URL you set it up as, and write it to `check_submission.py` using the `-o` option.

## Docker image

You specified a Docker image above, using the `TESTING_DOCKER_IMAGE` variable. You need to make sure this Docker image is available, and **ensure that it can run the `check_submission.py` script**.

The details of how to set up a Docker image are beyond this short tutorial, but plenty of tutorials are available online.

## GitLab runners

You'll need [GitLab runners](https://docs.gitlab.com/runner/) to run the automated tests. These might be provided by GitLab already, but, if so, they're likely overloaded. You can either use those already provided, or you can set up your own. Please read the documentation on how to [install](https://docs.gitlab.com/runner/install/) and [register](https://docs.gitlab.com/runner/register/) your own runners if that's the path you choose. You'll want to *install* your runners on Linux (likely), but should configure the [Docker executor](https://docs.gitlab.com/runner/executors/docker.html) to run the tests.

This section in `/etc/gitlab-runner/config.toml` works well for my GitLab runner installation:

    [[runners]]
        name = "your-name-goes-here"
        url = "https://git.ucsc.edu/"
        token = "REDACTED"
        executor = "docker"
        [runners.custom_build_dir]
        [runners.cache]
            [runners.cache.s3]
            [runners.cache.gcs]
            [runners.cache.azure]
        [runners.docker]
            tls_verify = false
            image = "ethanlmiller/ubuntu2204-clang14:latest"
            allowed_images = ["ethanlmiller/ubuntu2204-clang14:*"]
            pull_policy = "if-not-present"
            allowed_pull_policies = ["always", "if-not-present"]
            # Might want to tune the memory and CPUs policies
            cpus = 1
            memory = '2g'
            memory_swap = '8g'
            privileged = false
            disable_entrypoint_overwrite = false
            oom_kill_disable = false
            disable_cache = false
            volumes = ["/cache"]
            shm_size = 0

# Assignment rubrics

The `assignments.toml` file only lists all of the assignments and provides basic details on them, such as due dates. The detailed scripts for each assignment are stored separately in *rubric* TOML files, one per assignment. An assignment's rubric file is only loaded when the detailed tests are run; the basic check for minimal requirements is handled by `assignments.toml`.

## `assignments.toml`

This file can be named anything you want: its name is specified on the command line for `check_submission.py`. It contains a few overall configuration variables and a section for each assignment. You can add assignments over the term. The only requirement is that there be at most *one assignment active at any time*. It's OK to have zero assignments active at some time, but the script will only run tests for one assignment.

Each assignment uses a number of configuration parameters that can be set in its section. A default value for these variables should be set in the "general" section of the `.toml` file; if a specific assignment section doesn't set a configuration variable, it uses the default value.

The global variable is:

- `gitlab_server = 'https://git.ucsc.edu'`: The URL of the GitLab server.

The per-assignment variables are:

- `fromdate = "2022-08-31 00:01 PDT"`: This is the earliest date and time that a test script will be run for this assignment.
- `todate = "2022-09-28 11:59 PDT"`: This is the latest date and time that a test script will be run for this assignment.
- `directory = 'asgn0'`: Prepended to every file name or pattern specified in `prefiles` and `postfiles`.
- `prefiles = "!*.o !*.d !hello CHEATING-{reponame}.pdf PRD-{reponame}.pdf"`: Specifies the set of files that must (or must not) be present in the repository under `directory`. If a pattern or name is preceded by `!`, the pattern or name must *not* match. {reponame} is replaced with the name of the repository, which is usually the student's CruzID.
    Here, for example, `directory` must *not* contain any `.o` or `.d` files, and must not contain the file `hello`. It *must* contain `CHEATING-cruzid.pdf` and `PRD-cruzid.pdf`, assuming that the repo is named `cruzid`. All of these checks are done on the cloned repository *before* running any commands.
- `script = "cd asgn0 ; echo '-- Files in asgn0 before compile --' ; ls ; cc -o hello hello.c ; echo '-- Files in asgn0 after compile --' ; ls"`: This is the script that is run to ensure that `directory` in the repository is acceptable for submission. This might include (as in this example) that the code in the directory compiles properly.
    The script is a Python string, and can include newlines if it's enclosed by triple quotes (`"""`).
- `postfiles` = "hello CHEATING-{reponame}.pdf PRD-{reponame}.pdf": These are the files that must (or must not) be present in `directory` *after* running `script`. Typically, you don't want to check (again) for files that must *not* be present, since compilation will generate files that you wouldn't want in the repository. This is OK. But you *do* want to check to see that the executable is present --- this ensures that the build step worked.
- `rubric = "asgn0-rubric.toml"`: This will fetch the rubric for the assignment from the URL `$TESTING_BASE_URL/$TESTING_SECRET/asgn0-rubric.toml`. The `check_submission.py` script clips the name of the assignments config file from the URL and replaces it with the string in `rubric`. So, in this example, the URL for the assignment rubric is `https://www.soe.ucsc.edu/~cruzid/testing-stuff/abcdef0123456789/asgn0-rubric.toml`.

The following variables may be specified per-assignment, but are often specified globally since most assignments use the same values for them:

- `timeout = 10.0`: Specifies the timeout for running individual scripts. You might need to make this longer if your script takes a long time to run.
- `minimum_interval = 7200`: Specifies the minimum interval (in seconds) between runs of the assignment-specific rubric. This is used to reduce load on the GitLab runners, and to encourage students *not* to use the automated testing system as their primary debugging method.
- `patterns = "glob"`: Specifies pattern matching mechanism for files. Can either be `glob` (for shell-style `glob` matching) or `re` (for Python `re` matching). While you *can* override this per-assignment, there's usually no reason to.
- `stdout_size = 100`: Amount of standard output to provide for each test.
- `stderr_size = 0`: Amount of standard error to provide for each test.

The two remaining variables specify the messages saying "it's OK to submit" or "it's not OK to submit":

    submit_ok_msg = '''
    .......................................................
    ++++ Your submission has met minimum requirements for assignment {asgn}.
    ++++ You may submit commit ID {commit_id} for {asgn}.
    .......................................................'''

    submit_error_msg = '''
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!! ERRORS OCCURRED - check failed for {asgn}!
    !!!! Please see above for error messages.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'''

Again, you can change these per-assignment, but it's unclear why you'd want to do this.

## Per-assignment configuration files (*rubrics*)

A rubric file consists largely of individual tests. Each test has only a few configuration
entries:

- `points = 4`: This test is worth 4 points if it succeeds
- `tags = ['test', 'grade']`: This test is run if the `test` or `grade` flag is provided to the script. If you don't specify a tag, it defaults to `test`.
- `script = 'pdftotext asgn0/CHEATING-{user}.pdf - | grep -iwl {user}'`: This is a script to run for the test. The script can be multiline (it's a Python string), and it can use files fetched for use in the testing environment. See the sample assignment rubrics for details on fetching files for use in scripts.
- `description = "Ensure that CruzID appears in academic honesty document."`: This is a description of the test, printed to the test results for CI/CD testing and the rubric (uploaded to Canvas) for grading.
- `failure_msg = "Failure!"`
- `success_msg = "Success!"`: The `success_msg` and `failure_msg` messages are printed to the CI/CD output on success and failure, respectively.

# Git badges

The `check_submission.py` script provides badges in GitLab, which allow students to more easily access test results.
To do this, you'll need to set up a simple badge server.

The file `gitbadge.py` is a very simple CGI script that serves badges.
It uses the badge "templates" in the SVG files in the repository.
Each SVG file has a `{commitid}` placeholder in it that's replaced with the commit ID from the URL
that "queries" the badge server.

A badge URL is built from the environment variable you set earlier and two parameters passed to it.
For example, if `TESTING_BADGE_URL` is `https://users.soe.ucsc.edu/~elm/cgi-bin/gitbadge.py`, the
full URL of a badge might be `https://users.soe.ucsc.edu/~elm/cgi-bin/badge.py?f=pass&c=01234567`.
This would use the file `pass.svg` and replace `{commitid}` with `01234567`, delivering the
result as an SVG image to be displayed in the Web browser.

Obviously, `gitbadge.py` must be a CGI executable, and it must know where to find the SVG templates.
This is hardcoded into `gitbadge.py`, but the operating part of the script is less than 10 lines
long, and it should be obvious *which* line points to the SVG images. (I hope.)

The good news is that the badge server is completely stateless, and incredibly simple.
This means that it can be run pretty much anywhere with minimal risk.
Just make sure that the badge directory is a leaf.
The script ensures that the badge name is one of the four "allowable" names, and uses
`fail.svg` if not.

# Sample student directory

A [sample student directory](https://git.ucsc.edu/teaching-utils/repo_with_automated_testing) is available for testing. It contains a working `.gitlab-ci.yml` file, a basic `.gitignore` file, and several assignment directories with `README.md` files in them. The `asgn0` directory has a simple program in it. You can create a `samplestudent` repo in your class's group, and push the material in the sample student repository to it.

If you do so, and you've set up the rest of your infrastructure properly, you can see the results of both the automatic file checking (make sure the repo builds and has the right files) and of testing (it passes the single test in `assignment-testing.toml`) on this sample repo. This might be useful in seeing whether your basic setup is working *before* customizing `assignments.toml` for your class.


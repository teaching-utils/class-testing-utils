#!/usr/bin/env python3
#
# Check a submission for basic compliance with assignment.
#
# This is done by doing the following in the repo:
#
# 1. Change to the appropriate directory
# 2. Run the configured command
# 3. Check for the existence of a list of files.
#
# The directory, command, and list of files are gathered from a configuration
# file.  The config file can specify dates for each of a list of possible
# configurations, allowing the instructor to verify different assignments at
# different times.
#
# The script config can only be edited by someone with access to the machine on which
# CI is done.  However, the script config only specifies a directory prefix and a URL
# from which to pull the config file for a given repo.

import sys,os
import os.path
import signal
from datetime import datetime, timezone
from dateutil import parser as dtparser
from dateutil.tz import *
import io
from functools import reduce
import requests
import re
import csv
import configparser
import subprocess
import glob
import toml, json
import gitlab
from copy import deepcopy
from collections import namedtuple
from git import Repo
import svgwrite
from operator import and_, or_, add

TestResult = namedtuple('TestResult', 'test, description, points_recvd, points_avail')

repo_info = dict()

default_stderr_size = 200
default_stdout_size = 0
default_pattern_type = 'glob'
tests_job = 'runtests'
max_comment_length = 8000

class TestConfig:
    def __init__ (self, config_url):
        self.config_text = get_url_or_file (config_url)
        self.config_url = config_url
        self.config = toml.loads (self.config_text)
    def get (self, section, k, default_value = None):
        '''
        k: element key
        section: Element to search (dot-separated).
        default_value: value if key k not found

        Searches the config file, removing a component at a time, until the
        requested key is found. For example, if section='a.b.c', this
        will search:
        a.b.c
        a.b
        a
        top-level
        for the key, in that order, until the_key is found. If it's not
        found, default_value will be returned.
        '''
        try:
            if section != '':
                parts = section.split ('.')
                while (parts):
                    d = self.config
                    for p in parts:
                        d = d[p]
                    if k == '':
                        return deepcopy (d)
                    elif k in d:
                        return d[k]
                    parts = parts[:-1]
            # Throws error if key not defined anywhere
            if k == '':
                return deepcopy (self.config)
            else:
                return self.config[k]
        except:
            if default_value is None:
                raise
            return default_value




class Assignment:
    '''
    Collects all of the tests for a single assignment.
    '''
    def __init__ (self, asgn, config, vardict = None):
        """
        Load the basic info for this assignment.
        Don't load the rubric or any testing-related material now.
        Wait until it's explicitly requested.
        """
        self.config = config
        self.asgn = asgn
        if vardict:
            self.vardict = vardict.copy ()
        else:
            self.vardict = dict()
        # Load the assignment-wide config information
        self.start_date = dtparser.parse (self.config.get (self.asgn, 'fromdate'))
        self.end_date   = dtparser.parse (self.config.get (self.asgn, 'todate'))
        self.asgndir = self.config.get (self.asgn, 'directory')
        self.check_script = self.config.get (self.asgn, 'script')
        self.submit_ok_msg = self.config.get (self.asgn, 'submit_ok_msg')
        self.submit_error_msg = self.config.get (self.asgn, 'submit_error_msg')
        self.prefiles  = self.config.get (self.asgn, 'prefiles', '').format (**self.vardict)
        self.postfiles = self.config.get (self.asgn, 'postfiles', '').format (**self.vardict)
        self.check_type  = self.config.get (self.asgn, 'patterns', default_pattern_type)
        self.rubric_path = self.config.get (self.asgn, 'rubric')
        self.timeout = self.config.get (self.asgn, 'timeout')
        self.stdout_size = self.config.get (self.asgn, 'stdout_size', 100)
        self.minimum_interval = self.config.get (self.asgn, 'minimum_interval', 7200)
        self.user_exceptions = self.config.get (self.asgn, 'user_exceptions', 'sammyslug').split ()
        self.output = []

    def date_in_range (self):
        return self.start_date < datetime.now(tzlocal()) < self.end_date

    def prt (self, s):
        self.output.append (s)

    def check_minimum (self):
        self.prt ("\n========> Checking {0} now ({1}) ==================================".format (self.asgn, datetime.now(tzlocal()).strftime("%Y-%m-%d %H:%M:%S.%f")))
        self.prt ('------ Checking for files before running commands ------')
        pre_check_msgs = check_files (self.asgndir, self.prefiles.split (), self.check_type)
        if pre_check_msgs:
            self.prt (pre_check_msgs)
        else:
            self.prt ('Files OK')
        self.prt ('----- Running minimum requirements script:\n{0}'.format (self.check_script))
        # Run the commands
        p = subprocess.run (self.check_script, shell=True, check=False, executable='/bin/bash',
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        cmds_ok = (p.returncode == 0)
        self.prt ('------ Command output ------')
        try:
            self.prt (p.stdout.decode('utf8'))
        except:
            self.prt ("Output failed UTF-8 decoding!")
        self.prt ('------ End command output ------')
        self.prt ('------ Checking for files after running commands ------')
        post_check_msgs = check_files (self.asgndir, self.postfiles.split (), self.check_type)
        if post_check_msgs:
            self.prt (post_check_msgs)
        else:
            self.prt ('Files OK')
        self.requirements_ok = (not pre_check_msgs) and cmds_ok and (not post_check_msgs)
        if self.requirements_ok:
            self.prt (self.submit_ok_msg.format (**{**repo_info, 'asgn': self.asgn}))
        else:
            self.prt (self.submit_error_msg.format (**{**repo_info, 'asgn': self.asgn}))
        self.prt ("<======== Done checking {0} ({1})\n".format (self.asgn, datetime.now(tzlocal()).strftime ("%Y-%m-%d %H:%M:%S.%f")))
        return self.requirements_ok

    def fetch_rubric (self, rubric_base):
        self.tests = []
        full_rubric_path = os.path.join (rubric_base, self.rubric_path)
        self.r_config = TestConfig (full_rubric_path)
        self.pre_test_script = self.r_config.get ('', 'pre_script', '').format (**self.vardict)
        self.post_test_script = self.r_config.get ('', 'post_script', '').format (**self.vardict)
        self.scores_json_file = self.r_config.get('', 'scores_json_file', '')
        self.assignment_name = self.r_config.get('', 'asgn_name', self.asgn)
        self.rubric_field = self.r_config.get('', 'rubric_field', 'functionality')
        for t in self.r_config.get ('','test'):
            self.tests.append (SingleTest (self, 'test.{0}'.format (t)))
        return True

    def process_rubric (self, test_tag):
        # Reset output
        self.output = []
        self.rubric_run_at = datetime.now(timezone.utc).astimezone().strftime('%Y-%m-%d %H:%M %Z')
        self.status = 'run'
        self.pre_test_process = subprocess.run (self.pre_test_script, shell=True, check=False,
                                                executable='/bin/bash',
                                                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if self.pre_test_process.returncode != 0:
            self.output.append ('Pre-script failed for {0}:'.format (self.asgn))
            try:
                self.output.append (self.pre_test_process.stdout[:5000].decode('utf-8'))
            except:
                self.output.append ("Output failed UTF-8 decoding!")
            self.status = 'failed'
            return False
        for t in self.tests:
            t.run_test (test_tag)
            self.prt (t.output)
        self.post_test_process = subprocess.run (self.post_test_script, shell=True, check=False,
                                                 executable='/bin/bash',
                                                 stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        return True

    def get_results (self):
        results = [TestResult(t.key, t.description, t.points_recvd, t.points_avail)
                   for t in self.tests if t.status != 'skipped']
        return (results, sum([r.points_recvd for r in results]), sum([r.points_avail for r in results]))

    def get_results_as_csv (self):
        results, total_points_recvd, total_points_avail = self.get_results ()
        csv_string = io.StringIO ("")
        results_csv = csv.writer (csv_string, lineterminator='\n')
        results_csv.writerow (['Test'] + [r.test for r in results] + ['Total points'])
        results_csv.writerow (['Description'] + [r.description for r in results] + ["Total"])
        results_csv.writerow (['Available'] + [str(r.points_avail) for r in results] + [str(total_points_avail)])
        results_csv.writerow (['Received'] + [str(r.points_recvd) for r in results] + [str(total_points_recvd)])
        return csv_string.getvalue()

    def get_results_as_comments (self):
        results, test_total_recvd, test_total_avail = self.get_results ()
        return "\n".join (['Tests run on commit ID {0} at {1}'.format (repo_info['commit_id'], self.rubric_run_at)] +
                          ['{0} ({1}/{2}): {3}'.format (r.test, r.points_recvd, r.points_avail,
                                                        r.description) for r in results] +
                          ["Total: {0}/{1}".format(test_total_recvd, test_total_avail)]
                         )

    def update_scores_file (self, user):
        if not self.scores_json_file:
            return False
        try:
            with open (self.scores_json_file, "r") as f:
                scores = json.load (f)
        except:
            scores = dict()
        if self.asgn not in scores:
            scores[self.asgn] = dict()
        if 'scores' not in scores[self.asgn]:
            scores[self.asgn]['scores'] = dict()
        scores[self.asgn]['name'] = self.assignment_name
        scores[self.asgn]['rubric_field'] = self.rubric_field
        if self.status == 'failed':
            test_total_recvd = 0
            test_comments = '\n'.join (self.output)[:max_comment_length]
        else:
            results, test_total_recvd, test_total_avail = self.get_results ()
            test_comments = self.get_results_as_comments ()[:max_comment_length]
        scores[self.asgn]['scores'][user] = {'points': test_total_recvd,
                                             'comments': test_comments,
                                             'commit_id': repo_info['commit_id'],
                                             'rubric_run_at': self.rubric_run_at,
                                            }
        if self.status == 'run':
            scores[self.asgn]['scores'][user]['csv'] = self.get_results_as_csv()
        with open (self.scores_json_file, "w") as f:
            json.dump (scores, f, sort_keys=True, indent=1)
            f.write ('\n\n')
        return True

class SingleTest:
    def __init__ (self, assignment, key):
        self.assignment = assignment
        self.config = assignment.r_config
        self.key = key
        self.description = self.config.get (key, 'description', '')
        self.script = self.config.get (key, 'script').format (**self.assignment.vardict)
        self.success_msg = self.config.get (key, 'success_msg', 'success')
        self.failure_msg = self.config.get (key, 'failure_msg', 'failure')
        self.timeout = float (self.config.get (key, 'timeout', 10))
        self.points_avail = self.config.get (key, 'points', 1)
        self.points_recvd = 0
        self.tags = self.config.get (key, 'tags', '')
        self.status = 'not run'
        self.status_msg = 'not run'
        self.output = ''

    def run_test (self, tag):
        self.status = 'skipped'
        test_ok = True
        if tag not in self.tags:
            return False
        self.points_recvd = 0
        self.status = 'failure'
        self.status_msg = self.failure_msg
        self.stdout = ''
        try:
            process = subprocess.Popen (self.script, shell=True, executable='/bin/bash',
                                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                        start_new_session=True)
            self.stdout, errs = process.communicate(timeout=self.timeout)
        except subprocess.TimeoutExpired as e:
            self.status = 'timeout'
            self.status_msg = 'Process timed out after {0:.0f} seconds'.format (self.timeout)
            os.killpg(os.getpgid(process.pid), signal.SIGTERM)
            self.stdout, errs = process.communicate ()
        except Exception as e:
            self.status = 'crash'
            self.status_msg = 'Unexpected exception: ' + repr(e)
            test_ok = False
        else:
            if process.returncode == 0:
                self.points_recvd = self.points_avail
                self.status = 'success'
                self.status_msg = self.success_msg

        self.output = '-' * 60 + '\nTest: {0}\nDescription: {1} (timeout={5:.0f} seconds)\nResult: {2}\nPoints: {3}/{4}\n'.format (self.key, self.description, self.status_msg, self.points_recvd, self.points_avail, self.timeout)
        if self.status == 'failure' or self.status == 'timeout':
            self.output += '=========== Truncated output from failed test ===========\n'
        try:
            self.output += self.stdout[:self.assignment.stdout_size].decode ('utf8') + '\n'
        except:
            self.output += "Output failed UTF-8 decoding\n"
        self.output += '-' * 60 + '\n\n'
        return test_ok

# Globally-useful values
proj_id = os.environ.get ('CI_PROJECT_ID', '')
# Keep a copy of the testing secret and API token, but nuke the environment variables.
# We can always get it back via the config_url, which is definitely
# *not* passed to child processes.
testing_secret = os.environ.get ('TESTING_SECRET')
testing_api_token = os.environ.get ('TESTING_API_TOKEN')
os.environ['TESTING_SECRET'] = ''
os.environ['TESTING_API_TOKEN'] = ''

#######################################################################
#
# Badges? We *need* stinkin badges!
#
#######################################################################
badge_color = {
    'black': svgwrite.rgb (0,0,0,'%'),
    'white': svgwrite.rgb (100,100,100,'%'),
    'green': svgwrite.rgb (0,60,0,'%'),
    'ltgrn': svgwrite.rgb (60,100,60,'%'),
    'ltred': svgwrite.rgb (100,80,80,'%'),
    'red':   svgwrite.rgb (100,0,0,'%'),
    'yellow':svgwrite.rgb (100,100,0,'%'),
}

def create_submit_badge (commit_id):
    svg_name = 'submit_ok.svg'
    dwg = svgwrite.Drawing (svg_name, (520, 30))
    dwg.add (dwg.rect ((1,1), (518,28), rx=8, ry=8, stroke_width=2,
             stroke=badge_color['black'], fill=badge_color['green']))
    dwg.add (dwg.text (commit_id, insert=(125,21), stroke='none', fill=badge_color['white'],
             font_size='12pt', font_family='monospace'))
    dwg.add (dwg.text ('OK to submit:', insert=(5,21), stroke='none', fill=badge_color['white'],
             font_size='12pt', font_weight='bold', font_family='Arial'))
    dwg.save ()
    return svg_name

def create_last_badge (commit_id, ok_to_commit):
    svg_name = 'last_check.svg'
    dwg = svgwrite.Drawing (svg_name, (220, 30))
    fill_color = badge_color['ltgrn'] if ok_to_commit else badge_color['ltred']
    dwg.add (dwg.rect ((1,1), (218,28), rx=8, ry=8, stroke_width=2,
             stroke=badge_color['black'], fill=fill_color))
    dwg.add (dwg.text (commit_id[:9], insert=(115,21), stroke='none', fill=badge_color['black'],
             font_size='12pt', font_family='monospace'))
    dwg.add (dwg.text ('Last commit:', insert=(5,21), stroke='none', fill=badge_color['black'],
             font_size='12pt', font_weight='bold', font_family='Arial'))
    dwg.save ()
    return svg_name

def create_tests_badge (commit_id):
    svg_name = 'test_results.svg'
    dwg = svgwrite.Drawing (svg_name, (220, 30))
    dwg.add (dwg.rect ((1,1), (218,28), rx=8, ry=8, stroke_width=2,
             stroke=svg.rgb(0, 0 ,0, '%'), fill=badge_color['yellow']))
    dwg.add (dwg.text (commit_id[:9], insert=(115,21), stroke='none', fill=badge_color['black'],
             font_size='12pt', font_family='monospace'))
    dwg.add (dwg.text ('Test results:', insert=(5,21), stroke='none', fill=badge_color['black'],
             font_size='12pt', font_weight='bold', font_family='Arial'))
    dwg.save ()
    return svg_name

def update_check_badges (gl, commit_id, success, new_assignment):
    proj = gl.projects.get (proj_id)
    badges = proj.badges.list ()
    submit_ok_badge = None
    last_results_badge = None
    badge_url = os.environ['TESTING_BADGE_URL']
    job_url = os.environ['CI_JOB_URL']
    commit = commit_id[:8]
    for b in badges:
        if b.name == 'submit_ok':
            submit_ok_badge = b
        elif b.name == 'last_check':
            last_results_badge = b
    check_badge = 'pass' if success else 'fail'
    if last_results_badge:
        last_results_badge.link_url = job_url
        last_results_badge.image_url = badge_url.format (commit=commit, badge=check_badge)
        last_results_badge.save ()
    else:
        proj.badges.create ({'link_url': job_url,
                             'image_url' : badge_url.format (commit=commit, badge=check_badge),
                             'name': 'last_check'})
    if submit_ok_badge:
        # Update previous badge if we succeeded
        if success:
            submit_ok_badge.link_url = job_url
            submit_ok_badge.image_url = badge_url.format (commit=commit, badge='submitok')
            submit_ok_badge.save ()
        # Delete the previous badge if it's a new assignment
        elif new_assignment:
            submit_ok_badge.delete ()
    elif success:
        proj.badges.create ({'link_url': job_url,
                             'image_url': badge_url.format (commit=commit, badge='submitok'),
                             'name': 'submit_ok'})

def update_test_badge (gl, commit_id):
    proj = gl.projects.get (proj_id)
    badges = proj.badges.list ()
    badge_url = os.environ['TESTING_BADGE_URL']
    job_url = os.environ['CI_JOB_URL']
    commit = commit_id[:8]
    full_badge_url = badge_url.format (commit=commit, badge='tests')
    # print ("Full badge URL:", full_badge_url)
    for b in badges:
        if b.name == 'last_test':
            b.link_url = job_url
            b.image_url = full_badge_url
            b.save ()
            break
    else:
        # No badge found, so make one
        proj.badges.create ({'link_url': job_url,
                             'image_url': full_badge_url,
                             'name': 'last_test'})

#######################################################################
#
# Utility functions
#
#######################################################################

def get_url_or_file (url):
    """
    Get a file from either a filename or a URL.
    """
    if 'http://' in url or 'https://' in url:
        r = requests.get (url)
        r.raise_for_status ()
        txt = r.text
    else:
        with open (url) as f:
            txt = f.read ()
    return txt

#######################################################################
#
# check_files
#
# Check files for matching the passed pattern. This is similar to glob,
# but not identical, since it supports the ! operator for files that
# must *not* be present.
#
#######################################################################
def check_files_re (directory, exprs, verbose):
    msgs = []
    file_list = list_dir (directory, True)
    for e in exprs:
        if e[0] == '!':
            for f in file_list:
                if re.match (e[1:], f):
                    msgs.append ('ERROR: File {1} matches {0} - should not be present!'.format (e[1:], f))
        else:
            if not reduce (lambda x, y: x or y, [re.match (e, f) for f in file_list]):
                msgs.append ('ERROR: Expecting file to match {0} - none found!'.format (e))
    return '\n'.join (msgs)

def check_files_glob (directory, exprs, verbose):
    msgs = []
    for e in exprs:
        if e[0] == '!':
            files = glob.glob (os.path.join (directory, e[1:]))
            if len(files) > 0:
                for f in files:
                    msgs.append ('ERROR: File {1} matches {0} - should not be present!'.format (e[1:], f))
        else:
            files = glob.glob (os.path.join (directory, e))
            if len (files) == 0:
                msgs.append ('ERROR: Expecting file to match {0} - none found!'.format (e))
    return '\n'.join (msgs)

def check_files (directory, exprs, check_type = 'glob', verbose = True):
    '''
    Check all of the files in a directory to see if they match the passed pattern.
    '''
    if check_type == 're':
        return check_files_re (directory, exprs, verbose)
    return check_files_glob (directory, exprs, verbose)

def list_dir (d, recurse = False):
    if recurse:
        flist = list()
        for root, dirs, files in os.walk (d):
            # Trim leading ./ from files
            if root[:2] == './':
                root = root[2:]
            elif root == '.':
                root = ''
            flist += [os.path.join (root, f) for f in files]
        return flist
    return [e.name for e in os.scandir (d)]

#######################################################################
#
# seconds_since_last_test
#
# Relies *heavily* on environment variables passed in the GitLab pipeline.
#
#######################################################################
def seconds_since_last_test (gl):
    '''
    Determine the number of seconds since the previous test job.
    This is a job identified by 'runtests'.
    '''
    time_since_previous = 86400 * 7

    proj = gl.projects.get (proj_id)
    current_job_id = int (os.environ['CI_JOB_ID'])
    jobs = proj.jobs.list (iterator=True, order_by='id')
    # print ("I am job {0} in pipeline from {1}".format (current_job_id, os.environ['CI_PIPELINE_CREATED_AT']))
    for j in jobs:
        # print ("Job {0} created at {1}".format (j.id, j.created_at))
        # Skip the entry for the current job!
        if j.id == current_job_id:
            continue
        if j.stage == 'test':
            time_since_previous = (dtparser.parse (os.environ['CI_JOB_STARTED_AT']) - 
                                   dtparser.parse (j.created_at)).total_seconds()
            break
    # print ("Time since previous:", time_since_previous)
    return time_since_previous, j.created_at

#######################################################################
#
# main code
#
#######################################################################

if __name__ == '__main__':
    if len (sys.argv) < 3:
        print ("Usage: {0} <config url> <operation> [tag0[,tag1]]".format (sys.argv[0]))
        sys.exit (1)
    config_url = sys.argv[1]
    operation = sys.argv[2]
    test_tag = 'gitlab'
    if len (sys.argv) > 3:
        test_tag = sys.argv[3]
    scores_file = None
    try:
        config = TestConfig (config_url)
    except:
        print ("Couldn't fetch testing config file!")
        sys.exit (1)
    gitlab_server = config.get ('', 'gitlab_server')
    try:
        repo = Repo('.')
        m = re.search (r'/(?P<cls>[-_.a-zA-Z0-9]+)/(?P<user>[a-zA-Z0-9_]+)(\.git)?$', repo.remotes.origin.url)
        user = m.group ('user')
        project = m.group ('cls')
    except:
        print ('Unable to determine user from repo origin URL - exiting!')
        sys.exit (1)

    repo_info['user'] = user
    repo_info['reponame'] = user
    repo_info['commit_id'] = str(repo.head.commit)

    # Load all assignments in the config file
    asgns = dict()
    for asgn in sorted(config.get ('', 'asgn').keys()):
        asgns[asgn] = Assignment ("asgn.{0}".format (asgn), config, repo_info)
        if asgns[asgn].date_in_range ():
            current_assignment = asgns[asgn]

    exit_code = 0
    gl = None
    if operation == 'check':
        asgn_ok = current_assignment.check_minimum ()
        print ('\n'.join (current_assignment.output))
        if proj_id:
            gl = gitlab.Gitlab (os.environ['CI_SERVER_URL'], private_token=testing_api_token)
            gl.auth ()
            interval, last_job_start = seconds_since_last_test (gl)
            if asgn_ok:
                min_interval = current_assignment.minimum_interval
                create_submit_badge (repo_info['commit_id'])
                print ("Last full test was {0:.1f} seconds ago.".format (interval))
                if interval > min_interval or user in current_assignment.user_exceptions:
                    # Kick off a full test
                    print ('Starting a full test pipeline.')
                    r = requests.post (os.path.join (os.environ['CI_SERVER_URL'],
                                                     'api/v4/projects',
                                                     os.environ['CI_PROJECT_ID'],
                                                     'pipeline?ref=main'),
                                       headers={'PRIVATE-TOKEN': testing_api_token})
                    if not r.ok:
                        print ('Error {0} starting full test pipeline'.format (r.status_code))
                else:
                    print ('Not running full tests: less than {0} seconds since last test.'.format(min_interval))
            # Update badges regardless of whether check passed
            new_assignment = (dtparser.parse (os.environ['CI_JOB_STARTED_AT']) - \
                                              current_assignment.start_date).total_seconds () < interval
            update_check_badges (gl, repo_info['commit_id'], asgn_ok, new_assignment)
        exit_code = 0 if asgn_ok else 1
    elif operation == 'test':
        # Only need to do badge stuff and timing verification if in a pipeline
        if proj_id:
            gl = gitlab.Gitlab (os.environ['CI_SERVER_URL'], private_token=testing_api_token)
            gl.auth ()
            # Verify that test job isn't being triggered too soon!
            interval, last_job_start = seconds_since_last_test (gl)
            if interval <= current_assignment.minimum_interval and user not in current_assignment.user_exceptions:
                min_t = current_assignment.minimum_interval
                print ('Previous test at {0} - no testing for {1} seconds'.format (last_job_start, min_t))
                sys.exit (1)
        print ('Running tests with tag {0}'.format (test_tag))
        try:
            current_assignment.fetch_rubric (os.path.dirname(config_url))
        except:
            print ('ERROR: unable to fetch {0} rubric!'.format (current_assignment.asgn))
            print ('Output:')
            print (current_assignment.output)
            sys.exit (1)
        asgn_ok = current_assignment.process_rubric (test_tag)
        if not asgn_ok:
            print ('Pre-script failed - unable to run tests on {0}.'.format (current_assignment.asgn))
            print ('\n'.join (current_assignment.output))
        else:
            print ('\n'.join (current_assignment.output))
            if proj_id:
                update_test_badge (gl, repo_info['commit_id'])
            else:
                print ('=' * 10 + ' Testing results (CSV) ' + '=' * (40 - len(' Testing results (CSV) ')))
                print (current_assignment.get_results_as_csv())
                print ('=' * 10 + ' Test results as comments ' + '=' * (40 - len(' Test results as comments ')))
                print (current_assignment.get_results_as_comments())
                print ('=' * 50)
        current_assignment.update_scores_file (user)
    sys.exit (exit_code)
